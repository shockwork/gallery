import React, { Component, PropTypes } from 'react';
import {connect} from 'react-redux';
import classNames from 'classnames';

import ImageCard from '../components/ImageCard';
import Spinner from '../components/Spinner';
import Modal from '../components/Modal';

import {fetchImages, changeSearch, viewImage} from '../actions/gallery';

class GalleryContainer extends Component {

  constructor(props) {
    super(props);
    // Bind the events to the correct context.
    this.handleSearchInputChange = this.handleSearchInputChange.bind(this);
    this.onScroll = this.onScroll.bind(this);
  }

  componentWillMount() {
    const { dispatch } = this.props;
    dispatch(fetchImages());
  }

  componentDidMount() {
    window.addEventListener('scroll', this.onScroll, false);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.onScroll, false); // No hanging eventhandlers.
  }

  onScroll() {
    const {
      dispatch,
      is_fetching_images,
      current_page,
      total_pages
    } = this.props;

    const { gallery } = this.refs;

    // This can be handled much more smoothly but
    // would take a while to rewrite for something production worthy. But you get the gist of it
    const scrollPos = window.scrollY;
    const galleryHeight = gallery.clientHeight;
    const maxPixelsToBottom = 600;
    if (galleryHeight - scrollPos < maxPixelsToBottom && !is_fetching_images && current_page < total_pages) {
      dispatch(fetchImages());
    }
  }

  renderImages(images) {

    const imagesJsx = [];
    images.forEach((_image) => {
      imagesJsx.push(<ImageCard onClick={ this.onImageCardClick(_image) } {..._image} key={ _image.id }/>);
    });
    return imagesJsx;
  }

  onImageCardClick(_image) {
    const { dispatch } = this.props;
    return function imageClickHandler(event) {
      return dispatch(viewImage(_image));
    }.bind(this);
  }

  renderEndOfImagesOrNoFound() {
    const {is_fetching_images, total_pages, current_page} = this.props;
    if (!is_fetching_images && total_pages === current_page) {
      return (
        <div className='end-of-images'>
          {() => {
            if (total_pages === 0) {
              return (<h3>No images found, sorry :/</h3>)
            } else {
              return (<h3>No more images, sorry :(</h3>)
            }
          }()}
        </div>
      )
    }
  }

  handleSearchInputChange(event) {
    const { dispatch } = this.props;
    dispatch(changeSearch(event.target.value));
  }

  render() {
    const {
      images,
      is_fetching_images,
      search_filter,
      selected_image,      
    } = this.props;
    const spinner = (is_fetching_images) ? <Spinner /> : '';

    return(
      <div className='gallery' ref='gallery'>
        <Modal />
        <header>
          <h2>500px Gallery</h2>
          <div className='filter'>
            <input
              type='text'
              placeholder='search'
              onChange={ this.handleSearchInputChange }
              value={ search_filter }
            />
          </div>
        </header>
        <div className='gallery-images'>
          <div className='images-wrap'>
            {this.renderImages(images)}
            <div className='spinner-wrap'>
              {spinner}
            </div>
          </div>
          {this.renderEndOfImagesOrNoFound()}
        </div>
      </div>
    )
  }

}

function mapStateToProps(state) {

  const {
    images,
    is_fetching_images,
    current_page,
    total_pages,
    search_filter,
    selected_image,
  } = state.gallery;

  return {
    images,
    is_fetching_images,
    total_pages,
    current_page,
    search_filter,
    selected_image,
  }

}

export default connect(mapStateToProps)(GalleryContainer);
