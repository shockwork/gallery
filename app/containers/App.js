import React, { Component, PropTypes } from 'react';
import {connect} from 'react-redux';

import GalleryContainer from './GalleryContainer';

class App extends Component {

  render() {
    return(
      <GalleryContainer />
    )
  }

}

function mapStateToProps() {
  return {}
}

export default connect(mapStateToProps)(App);
