import {CONSUMER_KEY} from '../../config.json';
import superagent from 'superagent';
import Promise from 'bluebird';

const BASE_URL = 'https://api.500px.com/v1';

// Not the nicest, but we only use two endpoints..
function constructUrl(path, page) {
  return `${BASE_URL}${path}&image_size=600&consumer_key=${CONSUMER_KEY}&page=${page}`
}

const Photos = {
  popular: function (page) {
    return new Promise((resolve, reject) => {
      superagent
        .get(constructUrl('/photos?feature=fresh_day&exclude=nude&sort=rating', page))
        .set('Accept', 'application/json')
        .end((err, res) => {
          if (err) {
            return reject(err);
          }
          return resolve(res);
        })
    });
  },
  filter: function (page, term) {
    return new Promise((resolve, reject) => {
      superagent
        .get(constructUrl(`/photos/search?term=${encodeURIComponent(term)}&exclude=nude&sort=rating`, page))
        .set('Accept', 'application/json')
        .end((err, res) => {
          if (err) {
            return reject(err);
          }
          return resolve(res);
        })
    });
  }
}

export default Photos;
