import {CONSUMER_KEY} from '../../config.json';
import superagent from 'superagent';
import * as types from '../constants/ActionTypes';
import PhotoService from '../services/PhotoService';

function preFetchImages() {
  return {
    type: types.REQUEST_IMAGES,
  };
}

function handleResponse(res, dispatch) {
  const {body} = res;
  let { total_pages, current_page } = body;
  if (total_pages === 0 && current_page === 1) {
    current_page = 0; // 500px returns current page as 1 even if no results are found, dont like that
  }
  dispatch(receiveImages(body.photos, total_pages, current_page));
}

export function fetchImages() {
  return (dispatch, getState) => {
    const {gallery} = getState();
    const {search_filter} = gallery;
    let apiURL;
    dispatch(preFetchImages());

    if (!search_filter) {
      PhotoService.popular(gallery.current_page+1).then(res => handleResponse(res, dispatch));
    } else {
      PhotoService.filter(gallery.current_page+1, search_filter).then(res => handleResponse(res, dispatch));
    }

  }
}

export function receiveImages(images, total_pages, current_page) {

  return {
    type: types.RECEIVE_IMAGES,
    images,
    total_pages,
    current_page,
  };

}


// So we don't fetch images unless some time has passed.
let INPUT_TIMEOUT = null;

export function changeSearch(searchText) {

  return (dispatch, getState) => {
    // dispatch the search change.
    dispatch({
      type: types.FILTER_SEARCH_CHANGE,
      searchText,
    });

    if (INPUT_TIMEOUT) {
      clearTimeout(INPUT_TIMEOUT);
    }

    INPUT_TIMEOUT = setTimeout(() => {
      // If we are here 800ms has passed since last search change, trigger new fetch.
      dispatch(fetchImages());
    }, 800);
  }

}

export function viewImage(selected_image) {

  return {
    type: types.VIEW_IMAGE,
    selected_image: selected_image,    
  }

}
