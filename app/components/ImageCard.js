import React, { Component, PropTypes } from 'react';

import './imageCard.scss';

class ImageCard extends Component {
  render() {
    const { image_url, name, votes_count, user, onClick } = this.props;
    const imageStyle = {
      backgroundImage: `url(${image_url})`,
    };
    return(
      <div className='image-card' style={ imageStyle } onClick={ onClick }>
        <div className='inner'>
          <div className='center'>
            <em>{ name }</em>
            <span>votes: { votes_count }</span>
          </div>
          <div className='user-info'>
            <img className='avatar' src={ user.userpic_url } width='40px' />
            <p className='fullname'>{ user.fullname }</p>
          </div>
        </div>
      </div>
    )
  }
}

ImageCard.propTypes = {
  image_url: PropTypes.string.isRequired,
  name: PropTypes.string,
  votes_count: PropTypes.number,
  user: PropTypes.object,
  onClick: PropTypes.func,
}

export default ImageCard;
