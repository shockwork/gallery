import React, { Component } from 'react';
import classNames from 'classnames';
import { viewImage } from '../actions/gallery';
import {connect} from 'react-redux';

import './modal.scss';

class Modal extends Component {
  constructor(props) {
    super(props);
    // Bind the events to the correct context.
    this.handleModalBackgroundClick = this.handleModalBackgroundClick.bind(this);
  }
  handleModalBackgroundClick(event) {
    const { dispatch } = this.props;

    // Do not close the modal if the anchor within is clicked
    if (event.target.tagName.toLowerCase() === 'a')
      return;

    return dispatch(viewImage(null));
  }

  render() {
    const { selected_image } = this.props;
    const BASE_URL = 'https://500px.com';
    let { img, linkToPhoto } = '';
    if (selected_image) {
      img = <img src={ selected_image.image_url } />;
      linkToPhoto = <a target='_blank' href={BASE_URL + selected_image.url}>View on 500px</a>;
    }
    return (
      <div onClick={ this.handleModalBackgroundClick } className={classNames('modal-bkg', { show: selected_image })}>
        <div className='modal'>
          {img}
          {linkToPhoto}
        </div>
      </div>
    )
  }
}

function mapStateToProps(state) {

  const { selected_image } = state.gallery;

  return {
    selected_image,
  }

}

export default connect(mapStateToProps)(Modal);
