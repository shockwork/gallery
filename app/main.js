import App from './containers/App';
import React from 'react';
import ReactDOM from 'react-dom';
import configureStore from './store/configureStore';
import {Provider} from 'react-redux';

import './main.scss';

// this is a simple app so we don't check if it's production or development.. just
// include it for show
import DevTools from './containers/DevTools';

const store = configureStore();

ReactDOM.render(
  <Provider store={store}>
    <div>
      <App />
      <DevTools />
    </div>
  </Provider>,
  document.getElementById('main')
);
