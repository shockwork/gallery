import * as types from '../constants/ActionTypes';

const initialState = {
  images: [],
  total_pages: 0,
  current_page: 0,
  is_fetching_images: false,
  search_filter: '',
  selected_image: null,  
};

export default function gallery(state = initialState, action) {
  switch (action.type) {
    case types.REQUEST_IMAGES:
      return Object.assign({}, state, {
        is_fetching_images: true,
      });
    case types.RECEIVE_IMAGES:
      return Object.assign({}, state, {
        images: [...state.images, ...action.images],
        current_page: action.current_page,
        total_pages: action.total_pages,
        is_fetching_images: false,
      });
    case types.FILTER_SEARCH_CHANGE:
      return Object.assign({}, state, {
        current_page: 0,
        images: [],
        search_filter: action.searchText,
      });
    case types.VIEW_IMAGE:
      return Object.assign({}, state, {
        selected_image: action.selected_image,
      });
    default:
      return state;
  }
}
